﻿namespace TwitterTestClient
{
    public class AppKeys
    {
        public string RestUrl { get; set; }
        public string TokenHeaderKey { get; set; }
        public string ApiTweetRoute { get; set; }
        public string ApiSecurityRoute { get; set; }
    }
}
