﻿using Newtonsoft.Json.Linq;

namespace TwitterTestClient.Models
{
    public class SaveTweet
    {
        public JValue Description { get; set; }
        public JToken SaveValue { get; set; }
    }
}