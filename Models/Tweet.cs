﻿namespace TwitterTestClient.Models
{
    public class Tweet
    {
        public string text { get; set; }
        public string created_at { get; set; }
        public string screen_name { get; set; }
        public string name { get; set; }
        public string profile_image_url { get; set; }
    }
}