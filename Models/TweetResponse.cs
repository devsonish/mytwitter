﻿using System.Collections.Generic;

namespace TwitterTestClient.Models
{
    public class TweetResponse
    {
        public List<Tweet> tweetArray { get; set; }
        public string json { get; set; }
    }
}