﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using RestSharp;
using TwitterTestClient.Models;

namespace TwitterTestClient.Controllers
{
    [Route("api/v1.0/[controller]")]
    public class TwitterController : BaseController
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="appkeys">Key values</param>
        public TwitterController(IOptions<AppKeys> appkeys) : base(appkeys)
        {
        }

        [Route("Timeline/{username}")]
        public async Task<IActionResult> Timeline(string username)
        {
            try
            {
                //Initialize and call api...
                var response = await Helpers.GetResponseContentAsync(
                    RestClient(), 
                    RestRequest("SearchByHandle?handle=" + username, Method.GET, true ,null)) as RestResponse;

                //Get content...
                dynamic jtoken = JsonConvert.DeserializeObject(response.Content);

                //Get statuses...
                var statuses = jtoken.SelectToken("statuses");

                //Build tweets...
                var tweets = new List<Tweet>();
                foreach (var status in statuses)
                    tweets.Add(new Tweet
                    {
                        text = status.SelectToken("text").ToString(),
                        created_at = status.SelectToken("created_at").ToString(),
                        screen_name = status.SelectToken("user").SelectToken("screen_name").ToString(),
                        name = status.SelectToken("user").SelectToken("name").ToString(),
                        profile_image_url = status.SelectToken("user").SelectToken("profile_image_url").ToString()
                    });

                //Set response...
                var res = new TweetResponse
                {
                    tweetArray = tweets,
                    json = jtoken.ToString()
                };

                //Return response...
                return new ObjectResult(res);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("Search/{query}")]
        public async Task<IActionResult> Search(string query)
        {
            try
            {
                //Initialize and call api...
                var response = await Helpers.GetResponseContentAsync(
                    RestClient(),
                    RestRequest("Search?SearchTerm=" + query, Method.GET, true, null)) as RestResponse;

                //Get content...
                dynamic jtoken = JsonConvert.DeserializeObject(response.Content);

                //Get statuses...
                var statuses = jtoken.SelectToken("statuses");

                //Build tweets...
                var tweets = new List<Tweet>();
                foreach (var status in statuses)
                    tweets.Add(new Tweet
                    {
                        text = status.SelectToken("text").ToString(),
                        created_at = status.SelectToken("created_at").ToString(),
                        screen_name = status.SelectToken("user").SelectToken("screen_name").ToString(),
                        name = status.SelectToken("user").SelectToken("name").ToString(),
                        profile_image_url = status.SelectToken("user").SelectToken("profile_image_url").ToString()
                    });

                //Set response...
                var res = new TweetResponse
                {
                    tweetArray = tweets,
                    json = jtoken.ToString()
                };

                //Return response...
                return new ObjectResult(res);

            }
            catch (Exception)
            {
                throw;
            }
        }


        [Route("Save")]
        public async Task<IActionResult> Save([FromBody] SaveTweet saveTweet)
        {
            try
            {
                //Initialize and call api...
                var response = await Helpers.GetResponseContentAsync(
                    RestClient(),
                    RestRequest("Save", Method.PUT, true, saveTweet)) as RestResponse;


                //Return response...
                return new ObjectResult(response.StatusCode);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("GetSaved")]
        public async Task<IActionResult> GetSaved()
        {
            try
            {
                var tweetList = new List<TweetList>();

                //Initialize and call api...
                var response = await Helpers.GetResponseContentAsync(
                    RestClient(),
                    RestRequest("GetSaved", Method.GET, true, null)) as RestResponse;

                //Get content...
                dynamic jtoken = JsonConvert.DeserializeObject(response.Content);

                //Generate save list...
                foreach (var t in jtoken)
                    tweetList.Add(new TweetList
                    {
                        description = t.SelectToken("Description"),
                        id = t.SelectToken("Id")
                    });

                //Return response...
                return new ObjectResult(tweetList);

            }
            catch (Exception)
            {
                throw;
            }      
        }

        [Route("ClearSaved")]
        public async Task<IActionResult> ClearSaved()
        {
            try
            {
                //Initialize and call api...
                var response = await Helpers.GetResponseContentAsync(
                    RestClient(),
                    RestRequest("ClearSaved", Method.DELETE, true, null)) as RestResponse;

                //Return response...
                return new ObjectResult(response.StatusCode);

            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("Saved/{id}")]
        public async Task<IActionResult> GetSavedSearch(string id)
        {
            try
            {
                //Initialize and call api...
                var response = await Helpers.GetResponseContentAsync(
                    RestClient(),
                    RestRequest(id, Method.GET, true, null)) as RestResponse;

                //Get content...
                dynamic jtoken = JsonConvert.DeserializeObject(response.Content);
                dynamic j = JsonConvert.DeserializeObject(jtoken);

                //Get statuses...
                var statuses = j.SelectToken("statuses");
                
                //Build tweets...
                var tweets = new List<Tweet>();
                foreach (var status in statuses)
                    tweets.Add(new Tweet
                    {
                        text = status.SelectToken("text").ToString(),
                        created_at = status.SelectToken("created_at").ToString(),
                        screen_name = status.SelectToken("user").SelectToken("screen_name").ToString(),
                        name = status.SelectToken("user").SelectToken("name").ToString(),
                        profile_image_url = status.SelectToken("user").SelectToken("profile_image_url").ToString()
                    });

                //Set response...
                var res = new TweetResponse
                {
                    tweetArray = tweets,
                    json = jtoken.ToString()
                };

                //Return response...
                return new ObjectResult(res);

            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("Logon")]
        public async Task<IActionResult> Logon()
        {
            try
            {
                //Get username and password from header...
                Request.Headers.TryGetValue("Username", out StringValues param);
                var username = param;
                Request.Headers.TryGetValue("Password", out param);
                var password = param;

                //Initialize and call api...
                var response = await Helpers.GetResponseContentAsync(
                    RestClient(),
                    RestRequest("Token", Method.GET, username,password)) as RestResponse;

                string token = null;

                foreach (var header in response.Headers)
                    if (header.Name == "Token")
                        token = header.Value.ToString();

                if (token == null || string.IsNullOrEmpty(token))
                    throw new Exception();

                var tokenResponse = new Token
                {
                    token = token
                };

                return new ObjectResult(tokenResponse);
                //StringValues param;

                //Request.Headers.TryGetValue("Username", out param);
                //var username = param;

                //Request.Headers.TryGetValue("Password", out param);
                //var password = param;

                //var client = new RestClient("https://cvc-ws-dev.rims.ac.za");

                //// client.Authenticator = new HttpBasicAuthenticator(username, password);

                //var request = new RestRequest("/api/v1.0/Security/Token", Method.GET);

                //// easily add HTTP Headers
                //request.AddHeader("Authorization", "Basic " + Helpers.Base64Encode(username + ":" + password));

                //var response = new RestResponse();

                //response = await Helpers.GetResponseContentAsync(client, request) as RestResponse;

                //string token = null;

                //foreach (var header in response.Headers)
                //    if (header.Name == "Token")
                //        token = header.Value.ToString();

                //if (token == null || string.IsNullOrEmpty(token))
                //    throw new Exception();

                //var tokenResponse = new Token
                //{
                //    token = token
                //};

                //return new ObjectResult(tokenResponse);
            }
            catch (Exception)
            {   
                throw;
            }
        }
    }
}