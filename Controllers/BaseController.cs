using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using RestSharp;
using TwitterTestClient.Models;

namespace TwitterTestClient.Controllers
{
    public class BaseController : Controller
    {
        //App keys...
        public AppKeys AppKeys { get; set; }

        public BaseController()
        {
                
        }

        /// <summary>
        /// Constructor, set app keys
        /// </summary>
        /// <param name="appkeys"></param>
        public BaseController(IOptions<AppKeys> appkeys)
        {
            AppKeys = appkeys.Value;
        }

        /// <summary>
        /// Get the token from the header required for authentication
        /// </summary>
        /// <returns>Token</returns>
        public StringValues GetToken()
        {
            StringValues param;
            Request.Headers.TryGetValue(AppKeys.TokenHeaderKey, out param);
            return param;
        }

        /// <summary>
        /// Rest client with address
        /// </summary>
        /// <returns>RestClient</returns>
        public RestClient RestClient()
        {
            return new RestClient(AppKeys.RestUrl);
        }

        /// <summary>
        /// Manage request and set token if required
        /// </summary>
        /// <param name="apiParameters">Addtional API parameters</param>
        /// <param name="method">HTTP method type</param>
        /// <param name="setToken">Should the token be set</param>
        /// <param name="body">body</param>
        /// <returns></returns>
        public RestRequest RestRequest(string apiParameters, Method method, bool setToken, object body)
        {
            var request = new RestRequest(AppKeys.ApiTweetRoute + apiParameters, method);
            if (setToken)
                request.AddHeader(AppKeys.TokenHeaderKey, GetToken());

            if (body != null)
            {
                request.AddParameter("application/json", JsonConvert.SerializeObject(body), ParameterType.RequestBody);
            }

            return request;
        }

        /// <summary>
        /// Manage request with authorization
        /// </summary>
        public RestRequest RestRequest(string apiParameters, Method method, string userName, string password)
        {
            var request = new RestRequest(AppKeys.ApiSecurityRoute + apiParameters, method);
            request.AddHeader("Authorization", "Basic " + Helpers.Base64Encode(userName + ":" + password));
            return request;
        }
    }
}