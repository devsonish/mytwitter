﻿import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { LocalStorageService  } from 'angular-2-local-storage';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router,private localStorageService: LocalStorageService) {

    }
 
    canActivate() {
        if (this.localStorageService.get('token')) {
            // logged in so return true
            return true;
        }
        console.log(this.localStorageService.get('token'));
        // not logged in so redirect to login page
        this.router.navigate(['/login']);
        return false;

    }
}