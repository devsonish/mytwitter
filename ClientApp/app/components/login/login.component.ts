import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TwitterService } from '../_services/twitter.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],    
})
export class LoginComponent implements OnInit {

    private loading: boolean = false;

    constructor(private twitterService: TwitterService, private router: Router, private _flashMessagesService: FlashMessagesService, private localStorageService: LocalStorageService){}

    logon(username, password) {
        this.loading = true;
        this.twitterService.logon(username,password)
            .then(response => {
                if (response = true) {
                    this.redirect(); // Redirect to the stored url after user is logged in
                    this.loading = false;
                } else {
                    this._flashMessagesService.show('Error logging in', { cssClass: 'alert-danger', timeout: 3000 });
                    this.loading = false;
                }
            }).catch(error => { this._flashMessagesService.show('Error logging in', { cssClass: 'alert-danger', timeout: 3000 }); this.loading = false; });
    }

    private redirect(): void {
        this.router.navigate(["home"]);
    }

    ngOnInit() {
        if (this.localStorageService.get('token')) {
            // logged in so return true
            this.router.navigate(["home"]);
        }
    };
}
