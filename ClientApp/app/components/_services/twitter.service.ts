﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/Rx';
import { AsyncLocalStorage } from 'angular-async-local-storage';
import { TweetResponse, TweetList }  from '../shared/tweet/tweet.type'
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class TwitterService  {

    private token: any;

    constructor(private http: Http, private localStorageService: LocalStorageService) {
        this.token = this.localStorageService.get('token');
    }

    logon(username, password) {
        let headers = new Headers({
            'Username': username,
            'Password': password
        });
        return this.http.get('api/v1.0/Twitter/Logon', { headers: headers })
            .map(response => {
                let token = response.json() && response.json().token;
                    if (token) {
                        this.token = token;
                        this.localStorageService.set('token', token);
                    return true;
                    } else {
                        return false;
                    }
            })
            .toPromise();
    }

    getUserTweets(username) {
        let headers = new Headers({
            'Token': this.token
        });
        return this.http.get('api/v1.0/Twitter/Timeline/' + username, { headers: headers })
            .map(response => response.json() as TweetResponse)
            .toPromise();
    }

    searchTwitter(query) {
        let headers = new Headers({
            'Token': this.token
        });
        return this.http.get('api/v1.0/Twitter/Search/' + query, { headers: headers })
            .map(response => response.json() as TweetResponse)
            .toPromise();
    }

    getSavedTweetList() {
        let headers = new Headers({
            'Token': this.token
        });
        return this.http.get('api/v1.0/Twitter/GetSaved', { headers: headers })
            .map(response => response.json() as TweetList[])
            .toPromise();
    }

    getSavedSearch(id) {
        let headers = new Headers({
            'Token': this.token
        });
        return this.http.get('api/v1.0/Twitter/Saved/'+id, { headers: headers })
            .map(response => response.json() as TweetResponse)
            .toPromise();
    }

    clearSaved() {
        let headers = new Headers({
            'Token': this.token
        });
        return this.http.delete('api/v1.0/Twitter/ClearSaved/', { headers: headers })
            .toPromise().then(response => response.json())
            .catch(this.handleError);
    }

    saveTweets(description,json){
        let headers = new Headers({
        'Content-Type': 'application/json',
        'Token': this.token
        });
        let body = {
                'Description': description,
                'SaveValue': json,
        };
        return this.http.post('api/v1.0/Twitter/Save',body,{ headers: headers })
            .toPromise()
            .catch(this.handleError);
    }

    logout() {
        return this.localStorageService.remove('token');
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

}