﻿export class Tweet{
    text: string;
    created_at : string;
    screen_name : string;
    name: string;
    profile_image_url: string;
}

export class TweetList {
    description: string;
    id: string
}

export class TweetResponse{
    tweetArray:Tweet[];
    json:string
}
