﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TwitterService } from '../../_services/twitter.service';
import { Tweet, TweetList } from '../tweet/tweet.type';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
    selector: 'shared-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    private tweets: Tweet[] = [];
    private tweetList: TweetList[] = [];
    private json: string;
    private loading: boolean = false;

    constructor(private twitterService: TwitterService, private _flashMessagesService: FlashMessagesService,private router: Router) { }

    getUserTweets(username) {
        this.loading = true;
        this.twitterService.getUserTweets(username).then(
            res => {
                this.tweets = res.tweetArray;
                this.json = res.json;
                this.loading = false;
            }).catch(error => {
                this.errorMessage(`Unable to get user tweets: ${error}`);
                this.loading = false;
            });
    }

    searchTwitter(query) {
        this.loading = true;
        this.twitterService.searchTwitter(query).then(
            res => {
                this.tweets = res.tweetArray;
                this.json = res.json;
                this.loading = false;
            }).catch(error => {
                this.errorMessage(`Unable to search: ${error}`);
                this.loading = false;
            });
    }

    saveTweets(input: HTMLInputElement) {
        this.loading = true;
        this.twitterService.saveTweets(input.value, this.json)
            .then(res => {
                this.getSavedTweetList();
                input.value = null;
                this.successMessage('Search Saved!');
                this.loading = false;
            }).catch(error => {
                this.errorMessage(`Unable to save: ${error}`);
                this.loading = false;
            });
    }

    getSavedSearch(id) {
        this.loading = true;
        this.twitterService.getSavedSearch(id).then(
            res => { this.tweets = res.tweetArray; this.loading = false; }).catch(error => {
                this.errorMessage(error.Message);
                this.loading = false;
            });
    }

    clearSaved() {
        this.loading = true;
        this.twitterService.clearSaved()
            .then(res => {
                this.getSavedTweetList();
                this.successMessage("Saved Searches Cleared!")
                this.loading = false;
            }).catch(error => {
                this.errorMessage(`Unable clear saved tweets: ${error}`);
                this.loading = false;
            });
    }

    logout() {
        this.redirectLogin();
        this.twitterService.logout()  
    }

    private getSavedTweetList() {
        this.twitterService.getSavedTweetList().then(
            res => this.tweetList = res).catch(error => {
                this.errorMessage(error.Message);
            });
    }

    private successMessage(message) {
        this._flashMessagesService.show(message, { cssClass: 'alert-success', timeout: 3000 });
    }

    private errorMessage(message) {
        this._flashMessagesService.show(message, { cssClass: 'alert-danger', timeout: 3000 });
    }

    private redirectLogin(): void {
        this.router.navigate(["login"]);
    }


    ngOnInit() {
        this.getSavedTweetList();
    }
}


